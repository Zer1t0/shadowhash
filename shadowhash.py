#!/usr/bin/env python3

from shadowhash.__main__ import main

if __name__ == '__main__':
    exit(main())
